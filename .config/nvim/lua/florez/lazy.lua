local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

if not vim.g.vscode then

    require("lazy").setup(
        {
            {import = "florez.plugins"},
            {import = "florez.plugins.lsp"},
        }, {
            install = {
                colorscheme = { "rose-pine" },
            },
            change_detection = {
                notify = false,
            },
        }
    )

else

    require("lazy").setup({
        {import = "florez.plugins_vscode"},
        -- { import = "florez.plugins.nvim-treesitter" },
        -- { import = "florez.plugins.nvim-treesitter-text-objects" },
    })

end
