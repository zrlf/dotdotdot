return {
    'sindrets/diffview.nvim',
    lazy = true,
    cond = vim.g.vscode,
    config = true,
}
