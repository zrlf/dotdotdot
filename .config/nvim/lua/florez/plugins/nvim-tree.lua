local function my_on_attach(bufnr)
	local api = require("nvim-tree.api")

	local function opts(desc)
		return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
	end

	api.config.mappings.default_on_attach(bufnr)

	vim.keymap.set("n", "z", api.tree.change_root_to_node, opts("CD"))
end

return {
	"nvim-tree/nvim-tree.lua",
	dependencies = {
		"nvim-tree/nvim-web-devicons",
	},
	cond = vim.g.vscode,

	config = function()
		local nvimtree = require("nvim-tree")

		vim.g.loaded_netrw = 1
		vim.g.loaded_netrwPlugin = 1

		nvimtree.setup({
			on_attach = my_on_attach,
			view = {
				side = "left",
				width = 40,
			},
			update_cwd = true,
			update_focused_file = {
				-- enables the feature
				enable = true,
				-- update_cwd = true,
			},
			renderer = {
				special_files = {
					-- "Cargo.toml",
					-- "Makefile",
					-- "README.md",
					-- "readme.md",
					-- ".gitignore",
				},
			},
			actions = { open_file = { quit_on_open = true } },
			filters = { dotfiles = false, custom = { "^.DS_Store$", "^\\.git$" } },
			git = { enable = true, ignore = false, timeout = 500 },
		})

		-- vim.keymap.set("n", "<leader><tab>", ":NvimTreeToggle<CR>:only<CR>", silent)
		vim.keymap.set("n", "<leader><tab>", ":NvimTreeToggle<CR>", silent)
	end,
}
