return {
    'kdheepak/lazygit.nvim',
    dependencies = {
        'nvim-lua/plenary.nvim',
    },
    config = function()
        vim.keymap.set('n', '<leader>gg', ':LazyGit<Cr>', { noremap = true, silent = true })
        vim.keymap.set('n', '<leader>gf', ':LazyGitFilter<Cr>', { noremap = true, silent = true })
    end,
}
