return {
    'lervag/vimtex',
    cond = vim.g.vscode,
    lazy = false,
    config = function()
        vim.g['vimtex_view_method'] = 'zathura'

        vim.g['vimtex_syntax_enabled'] = 1
        vim.g['vimtex_complete_enabled'] = 1

        vim.g['vimtex_context_pdf_viewer'] = 'okular'
        vim.g['vimtex_syntax_conceal_disable'] = 1

    end,
    
}
