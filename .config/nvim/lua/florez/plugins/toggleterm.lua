return {
	{
		"akinsho/toggleterm.nvim",
		version = "*",
		config = function ()
		    require('toggleterm').setup{
                shade_terminals = false,
                -- direction = 'horizontal',
                direction = 'float',
                float_opts = {
                    border = 'curved',
                }
            }

            vim.keymap.set("n", "<C-/>", "<cmd>ToggleTerm<cr>")
            vim.keymap.set("t", "<C-/>", "<cmd>ToggleTerm<cr>")
		end,
	},
}
