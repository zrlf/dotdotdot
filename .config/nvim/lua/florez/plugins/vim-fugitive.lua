return {
    'tpope/vim-fugitive',
    lazy = false,
    cond = vim.g.vscode,
    config = function()
        vim.keymap.set("n", "<leader>gs", vim.cmd.Git);
        vim.keymap.set("n", "<leader>gd", vim.cmd.Gdiff);

    end,
}
