return {
    'epwalsh/obsidian.nvim',
    keys = "<leader>so",
    dependencies = {
        'nvim-lua/plenary.nvim',
        'nvim-treesitter/nvim-treesitter',
    },
    cond = vim.g.vscode,

    config = function()
        require('obsidian').setup({
            dir = "~/notes/Lorez/",
            daily_notes = {
                folder = "Daily notes",
            },
            templates = {
                subdir = "Templates",
            },
            completion = {
                nvim_cmp = true,
            }

        })

        vim.keymap.set("n", "<leader>so", ":ObsidianQuickSwitch<CR>")
    end
}
