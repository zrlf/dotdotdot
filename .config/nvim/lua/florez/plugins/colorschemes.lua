return {
	-- { "raddari/last-color.nvim" },
	{
		"rose-pine/neovim",
		priority = 1000,
		as = "rose-pine",
		config = function()
			require("rose-pine").setup({
				--- @usage 'auto'|'main'|'moon'|'dawn'
				variant = "auto",
				--- @usage 'main'|'moon'|'dawn'
				dark_variant = "main",
				bold_vert_split = false,
				dim_nc_background = false,
				disable_background = false,
				disable_float_background = true,
				disable_italics = false,
			})

			-- local theme = require("last-color").recall() or "rose-pine-moon"
			-- vim.cmd.colorscheme(theme)
			vim.keymap.set("n", "<leader>ct", function()
				if vim.o.background == "dark" then
					vim.cmd.colorscheme("rose-pine-dawn")
					vim.o.background = "light"
				else
					vim.cmd.colorscheme("rose-pine-moon")
					vim.o.background = "dark"
				end
			end)
		end,
	},
    {
        'rebelot/kanagawa.nvim',
    },
    {
        'folke/tokyonight.nvim',
    }
}
