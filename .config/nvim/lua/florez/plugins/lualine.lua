return {
    'nvim-lualine/lualine.nvim',
    priority = 100,
    cond = vim.g.vscode,
    config = function()
        require('lualine').setup {
            options = {
                icons_enabled = true,
                theme = 'auto',
                component_separators = '>',
                section_separators = '',
            },
        }
    end,
}
