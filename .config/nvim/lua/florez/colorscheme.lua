local function read_theme()
    local file = io.open(os.getenv("HOME").."/.color_mode", "r")
    if not file then return "dark" end
    local theme = file:read("*all")
    file:close()
    theme = theme:match("^%s*(.-)%s*$")
    return theme
end

local color_mode = read_theme()
print("Color mode: " .. color_mode)
if color_mode == "dark" then
    vim.cmd.colorscheme("rose-pine-moon")
    vim.o.background = "dark"
else
    vim.cmd.colorscheme("rose-pine-dawn")
    vim.o.background = "light"
end

-- float colors
-- vim.api.nvim_set_hl(0, "Pmenu", { fg = "#ebbcba" })
-- vim.api.nvim_set_hl(0, "PmenuSel", { bg = "NONE", fg = "#f5e0dc", italic = true, bold = true })
-- vim.api.nvim_set_hl(0, "docs", { fg = "#b4befe" })
--
-- vim.api.nvim_set_hl(0, "CmpItemAbbr", { fg = "#7f849c" })
-- vim.api.nvim_set_hl(0, "CmpItemAbbrMatch", { fg = "#f5e0dc" })
-- vim.api.nvim_set_hl(0, "CmpItemAbbrMatchFuzzy", { fg = "#bac2de" })
