return {
    {
        "kylechui/nvim-surround",
        version = "*", -- Use for stability; omit to use `main` branch for the latest features
        config = true,
    },
    {
        'terrortylor/nvim-comment',
        config = function ()
            require('nvim_comment').setup()
        end,
    },
}
