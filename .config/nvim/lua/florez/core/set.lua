vim.opt.nu = true
vim.opt.relativenumber = true
vim.opt.mouse = 'a'
vim.g.skip_ts_context_commentstring_module = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.undodir = os.getenv("HOME") .. "/.vim/undodir"
vim.opt.undofile = true

vim.opt.hlsearch = false
vim.opt.incsearch = true

vim.opt.scrolloff = 6

vim.opt.updatetime = 100

vim.opt.completeopt = 'menuone,noselect'

vim.g.vim_markdown_folding_disabled = 1
vim.g.vim_markdown_math = 1
vim.opt.conceallevel = 0

vim.opt.termguicolors = true

vim.g.copilot_filetypes = {
    markdown = true,
}

vim.filetype.add( { extension = { mdx = 'markdown'}})
