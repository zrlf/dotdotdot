local silent = { silent = true }

vim.g.mapleader = " "

vim.g.maplocalleader = "\\"

-- move selection/line up an down
vim.keymap.set("v", "<A-j>", ":m '>+1<CR>gv=gv")
vim.keymap.set("v", "<A-k>", ":m '<-2<CR>gv=gv")
vim.keymap.set("n", "<A-j>", "<S-V> :m '>+1<CR>gv=gv <Esc>")
vim.keymap.set("n", "<A-k>", "<S-V> :m '<-2<CR>gv=gv <Esc>")

-- ctrl s for save
vim.keymap.set("n", "<C-s>", ":w <CR>")
vim.keymap.set("i", "<C-s>", "<Esc>:w <CR>li")

-- paste from clipboard and replace paste
vim.keymap.set("x", "<leader>p", "\"_dP")
vim.keymap.set("n", "<leader>b", "\"+p")

-- copy to clipboard
vim.keymap.set("n", "<leader>y", "\"+y")
vim.keymap.set("v", "<leader>y", "\"+y")
vim.keymap.set("n", "<leader>Y", "\"+Y")

-- esc aliases
vim.keymap.set("i", "<C-c>", "<Esc>")
vim.keymap.set("i", "<A-c>", "<Esc>")
vim.keymap.set("n", "Q", "<nop>")

-- replace all occurences of word under cursor
vim.keymap.set("n", "<leader>s", [[:%s/\<<C-r><C-w>\>/<C-r><C-w>/gI<Left><Left><Left>]])

-- remove word
vim.api.nvim_set_keymap('i', '<C-H>', '<C-w>', {noremap = true})
vim.api.nvim_set_keymap('i', '<C-BS>', '<C-w>', {noremap = true})

-- change windows
vim.keymap.set("n", "<leader>w", "<C-w>")

-- chdir to current file
vim.keymap.set('n', '<leader>cd', ':cd %:p:h<CR>:pwd<CR>')
