-- Using Lua to set foldmethod and foldexpr with Treesitter
vim.api.nvim_exec(
	[[
  set foldexpr=nvim_treesitter#foldexpr()
]],
	false
)

-- set foldmethod=expr
