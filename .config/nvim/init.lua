
require("florez.core")
require("florez.lazy")
if not vim.g.vscode then
    require("florez.colorscheme")
end

vim.g.transparent_enabled = true

if vim.g.vscode then
    vim.opt.clipboard:append("unnamedplus")
end

if vim.g.neovide then

  vim.o.guifont = "MesloLGS Nerd Font:h12"
  vim.g.neovide_padding_top = 40
  vim.g.neovide_padding_bottom = 0
  vim.g.neovide_padding_right = 30
  vim.g.neovide_padding_left = 30

  vim.g.neovide_transparency = 0.95
  vim.g.background_color = "#1e1e2e"

  vim.g.neovide_floating_blur_amount_x = 20.0
  vim.g.neovide_floating_blur_amount_y = 2.0
  vim.g.transparent_enabled = false

  vim.g.neovide_scale_factor = 0.90
  -- vim.g.neovide_scale_factor = 1.0
  vim.opt.linespace = 2
  vim.g.neovide_refresh_rate = 60

  vim.api.nvim_set_keymap("n", "<C-+>", ":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor + 0.1<CR>", { silent = true })
  vim.api.nvim_set_keymap("n", "<C-->", ":lua vim.g.neovide_scale_factor = vim.g.neovide_scale_factor - 0.1<CR>", { silent = true })
  vim.api.nvim_set_keymap("n", "<C-0>", ":lua vim.g.neovide_scale_factor = 1<CR>", { silent = true })
end
