# --- Functions ---
function mount_euler {
  source ~/bin/utilities/mount_euler.sh "$1"
}

function mount_nas_home {
  source ~/bin/utilities/mount_nas_home.sh
}

function fcd {
  local current_dir=$(pwd)
  new_dir=$(fd --type d --exclude .git -H | fzf-tmux -p --reverse)

  # exit code 130 -> ctrl-c, 1 -> esc
  # if [ $? -eq 130 ] || [ $? -eq 1 ]; then
  #   cd $current_dir
  # else
  #   cd $new_dir
  # fi
  if [ $? -eq 0 ]; then
    cd $new_dir
  else
    cd $current_dir
  fi
}

# --- Aliases ---
alias fd="fdfind"
alias vd="fd --type f --exclude .git -H | fzf-tmux -p --reverse | xargs nvim"
alias euler="ssh florez@euler.ethz.ch"
alias umounteuler="umount ~/mnt/WORK; umount ~/mnt/HOME"
alias py="python3"
alias jupyter_euler='bash ~/resources/scripts/euler_jupyter/start_jupyter_for_vsc.sh'
alias jupyter_euler_browser='bash ~/resources/scripts/euler_jupyter/start_jupyter_for_vsc.sh --browser'
alias matlab='bash /usr/local/MATLAB/R2023a/bin/matlab'
alias nvim='/home/florez/dev/nvim.appimage'
alias vim=nvim
alias lvim='vim -c "normal '\''0"'
alias vid='neovide --frame none --neovim-bin ~/dev/nvim.appimage'
