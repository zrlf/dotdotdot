function cdn() {
  nautilus "$1" &>/dev/null &
}

bindkey '^[^?' backward-kill-word
bindkey '^ ' autosuggest-accept

bindkey '\t' menu-select "$terminfo[kcbt]" menu-select
bindkey -M menuselect '\t' menu-complete "$terminfo[kcbt]" reverse-menu-complete
